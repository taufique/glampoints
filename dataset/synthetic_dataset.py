import numpy as np
import cv2
import os
import random
from dataset.data_augmentation import random_brightness, random_contrast, additive_speckle_noise, \
    additive_gaussian_noise, add_shade, motion_blur
from dataset.homo_generation import homography_sampling


def apply_augmentations(image, augmentations, seed=None):
    def apply_augmentation(image, aug, augmentations, seed=None):
        '''
        arguments: image - gray image with intensity scale 0-255
                    aug - name of the augmentation to apply
                    seed
        output:
                    image - gray image after augmentation with intensity scale 0-255
        '''
        if aug == 'additive_gaussian_noise':
            image, kp = additive_gaussian_noise(image, [], seed=seed,
                                                std=(augmentations['additive_gaussian_noise']['std_min'],
                                                     augmentations['additive_gaussian_noise']['std_max']))
        if aug == 'additive_speckle_noise':
            image, kp = additive_speckle_noise(image, [], intensity=augmentations['additive_speckle_noise']['intensity'])
        if aug == 'random_brightness':
            image, kp = random_brightness(image, [], seed=seed)
        if aug == 'random_contrast':
            image, kp = random_contrast(image, [], seed=seed)
        if aug == 'add_shade':
            image, kp = add_shade(image, [], seed=seed)
        if aug == 'motion_blur':
            image, kp = motion_blur(image, [], max_ksize=augmentations['motion_blur']['max_ksize'])
        if aug == 'gamma_correction':
            # must be applied on image with intensity scale 0-1
            maximum = np.max(image)
            image_preprocessed = image / maximum
            random_gamma = random.uniform(augmentations['gamma_correction']['min_gamma'], \
                                          augmentations['gamma_correction']['max_gamma'])
            image_preprocessed = image_preprocessed ** random_gamma
            image = image_preprocessed * maximum
        if aug == 'opposite':
            # must be applied on image with intensity scale 0-1
            maximum = np.max(image)
            image_preprocessed = image / maximum
            image_preprocessed = 1 - image_preprocessed
            image = image_preprocessed * maximum
        if aug == 'no_aug':
            pass
        return image

    random.seed(seed)
    list_of_augmentations = augmentations['augmentation_list']
    index = random.sample(range(len(list_of_augmentations)), 3)
    for i in index:
        aug = list_of_augmentations[i]
        image = apply_augmentation(image, aug, augmentations, seed)
    image_preprocessed = image / (np.max(image) + 0.000001)
    return image, image_preprocessed


def sample_homography(image, sample_homography, seed=None):
    param = 0.0009
    if seed is None:
        random_state = np.random.RandomState(None)
    else:
        random_state = np.random.RandomState(seed)
        random.seed(seed)

    homography_perspective = np.array([[1 - param + 2 * param * random_state.rand(),
                                        -param + 2 * param * random_state.rand(),
                                        -param + 2 * param * random_state.rand()],
                                       [-param + 2 * param * random_state.rand(),
                                        1 - param + 2 * param * random_state.rand(),
                                        -param + 2 * param * random_state.rand()],
                                       [-param + 2 * param * random_state.rand(),
                                        -param + 2 * param * random_state.rand(),
                                        1 - param + 2 * param * random_state.rand()]])

    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    y = random.randint(-sample_homography['max_rotation'], sample_homography['max_rotation'])
    # perform the rotation
    M = cv2.getRotationMatrix2D(center, y, 1.0)
    homography_rotation = np.concatenate([M, np.array([[0, 0, 1]])], axis=0)

    tx = random.randint(-sample_homography['max_horizontal_dis'], \
                        sample_homography['max_horizontal_dis'])
    ty = random.randint(-sample_homography['max_vertical_dis'], \
                        sample_homography['max_vertical_dis'])
    homography_translation = np.eye(3)
    homography_translation[0, 2] = tx
    homography_translation[1, 2] = ty

    final_homography = np.matmul(homography_perspective, np.matmul(homography_rotation, homography_translation))
    image_transformed = cv2.warpPerspective(np.uint8(image), final_homography, (image.shape[1], image.shape[0]))
    return image_transformed, final_homography


def augment_images(original_image, cfg, seed=None):
    '''from original_image, create two sets of images: image1 and image2 related by
    a homography. they are both the results of a transformation of original image
    according to a randomly sampled homography and are augmented with appearance
    transformations.
    arguments: original_images - batch of images N x H x W x 1, intensity scale 0-255
               cfg - config
               seed
    outputs:    image1_batch - batch of image1 N x H x W x 1, intensity scale 0-255
                image2_batch - batch of image1 N x H x W x 1, intensity scale 0-255
                image1_norm_batch - batch of image1 (normed) N x H x W x 1, intensity scale 0-1
                image2_norm_batch - batch of image2 (normed) N x H x W x 1, intensity scale 0-1
                homography_batch - batch of homography each relating image1 to image2 N x 3 x 3
    '''
    image1_batch = np.zeros((original_image.shape))
    image2_batch = np.zeros((original_image.shape))
    image1_norm_batch = np.zeros((original_image.shape))
    image2_norm_batch = np.zeros((original_image.shape))
    homography_batch = np.zeros((original_image.shape[0], 3, 3))
    for i in range(original_image.shape[0]):
        image = original_image[i, :, :, 0]
        h1 = homography_sampling(image.shape, cfg['sample_homography'], seed=seed * (i + 1))
        image1 = cv2.warpPerspective(np.uint8(image), h1, (image.shape[1], image.shape[0]))
        image1, image1_preprocessed = apply_augmentations(image1, cfg['augmentation'], seed=seed * (i + 1))
        image1_batch[i, :, :, 0] = image1
        image1_norm_batch[i, :, :, 0] = image1_preprocessed

        # image2
        h2 = homography_sampling(image.shape, cfg['sample_homography'], seed=seed * (i + 2))
        image2 = cv2.warpPerspective(np.uint8(image), h2, (image.shape[1], image.shape[0]))
        image2, image2_preprocessed = apply_augmentations(image2, cfg['augmentation'], seed=seed * (i + 2))
        image2_batch[i, :, :, 0] = image2
        image2_norm_batch[i, :, :, 0] = image2_preprocessed

        H = np.matmul(h2, np.linalg.inv(h1))
        homography_batch[i] = H
    return image1_batch, image2_batch, image1_norm_batch, image2_norm_batch, homography_batch


def read_from_file(path, augmentation, mask):
    '''
    From a directory given in path containing images, this function create
    a matrix containing all the images (N).
    arguments:
                path - path to directory containing images
                mask - boolean if a mask of the foreground of the umages needs to be computed
                augmentation - dictionnary from config giving info on augmentation
    outputs:
                batch - image N x H x W x 1  intensity scale 0-255
                if mask is True
                also mask N x H x W x 1

    '''
    nbr_image = len(os.listdir(path))

    size1 = []
    size2 = []
    for f in (os.listdir(path)):
        current_path = os.path.join(path, f)
        im = cv2.imread(current_path)
        if im is not None:
            size1.append(im.shape[0])
            size2.append(im.shape[1])
    size = (np.max(size1), np.max(size2))

    if mask:
        batch_mask = np.zeros((nbr_image, size[0], size[1], 1), dtype=np.float32)
    batch = np.zeros((nbr_image, size[0], size[1], 1), dtype=np.float32)
    for i, name in enumerate(os.listdir(path)):
        try:
            image_full = cv2.imread('{}/{}'.format(path, name))
            if augmentation['use_green_channel']:
                image = image_full[:, :, 1]
            else:
                image = cv2.cvtColor(image_full, cv2.COLOR_BGR2GRAY)

            batch[i, :image.shape[0], :image.shape[1], 0] = image
            if mask:
                mask = (image < 230) & (image > 25)
                batch_mask[i, :image.shape[0], :image.shape[1], 0] = mask.astype(int)
        except:
            continue
    if mask:
        return batch, batch_mask
    else:
        return batch