# GLAMpoints: Greedily Learned Accurate Match points

This is the (unoptimized) reference implementation of the [ICCV 2019 paper "GLAMPoints: Greedily Learned Accurate Match points"](https://www.retinai.com/glampoints) ([arXiv](https://arxiv.org/pdf/1908.06812.pdf), [supplemental](https://static1.squarespace.com/static/5967a5599de4bb65a7bb9736/t/5d7a6d2b9e03815777188115/1568304461382/Supplementary_Material.pdf)). It enables the training of a domain-specific keypoint detector over non-differentiable registration methods. This code exemplifies the presented method using root SIFT, a homography model and RANSAC optimization.

## Training

To train the model on your dataset, prepare a folder with images (of approximately same height and width). Configure the file `config_training.yml` with at least `write_dir` and `path_images_training`. Adapt the hyperparameters in `training` to your dataset. With this setup, run the script:

```bash
python training_glam_detector.py --path_ymlfile config_training.yaml --plot True
```

## Testing

To test the pretrained model use the following script:

```bash
python compute_glam_kp.py --path_images /path/to/training/images/ --write_dir ///path/to/results/folder --NMS 10 --path_glam_weights /Users/dezanets/dev/glampoints/weights/model-34 --green_channel True
```

Outputs of this script are figures, showing for each image the detected keypoints. These figures are saved in --write_dir. It can also be a text file with the kp saved. 
The parameters are the following:
* --path_images can be path to a h5 file, a directory of images or a video (use codec). 
* --write_dir designates the directory where to save the images with the keypoints drawn on them
* --path_glam_weights is the path to the weights of the trained Reti model. 

optional arguments are
* --NMS which is the non max suppression window, default is 10
* --min_prob which is the minimum probability that a pixel must have to be considered as a keypoint on the score map (output of the Reti model), default is 0
* --green_channel: bool to determine if we are using the green channel instead of gray image to compute the score map (default is True).
* --save_text: bool to save a text file containing the matrix of kp extracted for each image. (default: False)