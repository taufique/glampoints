#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 14:42:15 2018

@author: truongp
"""
import numpy as np
import argparse
import tensorflow as tf
from skimage.feature import peak_local_max
import cv2
import os


def draw_keypoints(img, kp):
    '''
        arguments: gray images
                kp1 is shape Nx2, N number of feature points, first point in horizontal direction
    '''
    image_copy = np.copy(img)
    nbr_points = kp.shape[0]
    for i in range(nbr_points):
        image = cv2.circle(image_copy, (np.int32(kp[i, 0]), np.int32(kp[i, 1])), 1, (0, 0, 0), thickness=0)
    return image


def conv_conv_pool(input_layer, filters, training, pool, batch_norm, name):
    np.random.seed(0)
    np.random.RandomState(0)

    if batch_norm is True:
        conv1 = tf.layers.conv2d(inputs=input_layer, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib \
                                 .layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_1'.format(name))
        batch_conv1 = tf.layers.batch_normalization(conv1, training=training, name='batch_{}_1'.format(name))
        relu1 = tf.nn.relu(batch_conv1, name="relu_{}_1".format(name))
        conv2 = tf.layers.conv2d(inputs=relu1, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib. \
                                 layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_2'.format(name))
        batch_conv2 = tf.layers.batch_normalization(conv2, training=training, name='batch_{}_2'.format(name))
        relu2 = tf.nn.relu(batch_conv2, name="relu_{}_2".format(name))
    else:

        conv1 = tf.layers.conv2d(inputs=input_layer, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib \
                                 .layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_1'.format(name))
        relu1 = tf.nn.relu(conv1, name="relu_{}_1".format(name))
        conv2 = tf.layers.conv2d(inputs=relu1, filters=filters, kernel_size=[3, 3], \
                                 padding="same", activation=None, kernel_regularizer=tf.contrib. \
                                 layers.l2_regularizer(0.1), trainable=True, \
                                 kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                                 name='conv_{}_2'.format(name))
        relu2 = tf.nn.relu(conv2, name="relu_{}_2".format(name))

    if pool is False:
        return relu2
    else:
        pool = tf.layers.max_pooling2d(inputs=relu2, pool_size=[2, 2], strides=2, \
                                       padding='SAME', name='pool_{}'.format(name))
        return relu2, pool


def upconv_concat(input_A, input_B, n_filter, training, batch_norm, name):
    """Upsample `inputA` and concat with `input_B`
    Args:
        input_A (4-D Tensor): (N, H, W, C)
        input_B (4-D Tensor): (N, 2*H, 2*H, C2)
        name (str): name of the concat operation
    Returns:
        output (4-D Tensor): (N, 2*H, 2*W, n_filter + C2)
    """

    upscale = tf.layers.conv2d_transpose(inputs=input_A, filters=n_filter, kernel_size=[2, 2], \
                                         padding='same', strides=[2, 2], kernel_regularizer= \
                                             tf.contrib.layers.l2_regularizer(0.1), data_format='channels_last', \
                                         activation=None, trainable=True, use_bias=True, \
                                         kernel_initializer=tf.glorot_normal_initializer(),
                                         name='decon_{}'.format(name))

    A_shape = tf.shape(upscale)
    B_shape = tf.shape(input_B)

    # offsets for the top left corner of the crop
    offsets = tf.cast([0, (A_shape[1] - B_shape[1]) // 2, (A_shape[2] - B_shape[2]) // 2, 0], tf.int32)
    size = tf.cast([-1, B_shape[1], B_shape[2], n_filter], tf.int32)
    A_crop = tf.slice(upscale, offsets, size)

    concat = tf.concat([A_crop, input_B], axis=3, name="concat_{}".format(name))
    return concat


def Unet_model_4(input_layer, training, norm):
    conv1, pool1 = conv_conv_pool(input_layer, 8, training, pool=True, batch_norm=norm, name='1')
    conv2, pool2 = conv_conv_pool(pool1, 16, training, pool=True, batch_norm=norm, name='2')
    conv3, pool3 = conv_conv_pool(pool2, 32, training, pool=True, batch_norm=norm, name='3')
    conv4, pool4 = conv_conv_pool(pool3, 64, training, pool=True, batch_norm=norm, name='4')
    conv5 = conv_conv_pool(pool4, 128, training, pool=False, batch_norm=norm, name='5')
    up6 = upconv_concat(conv5, conv4, 64, training, batch_norm=norm, name='6')
    conv6 = conv_conv_pool(up6, 64, training, pool=False, batch_norm=norm, name='6')
    up7 = upconv_concat(conv6, conv3, 32, training, batch_norm=norm, name='7')
    conv7 = conv_conv_pool(up7, 32, training, pool=False, batch_norm=norm, name='7')
    up8 = upconv_concat(conv7, conv2, 16, training, batch_norm=norm, name='8')
    conv8 = conv_conv_pool(up8, 16, training, pool=False, batch_norm=norm, name='8')
    up9 = upconv_concat(conv8, conv1, 8, training, batch_norm=norm, name='9')
    conv9 = conv_conv_pool(up9, 8, training, pool=False, batch_norm=norm, name='9')
    output = tf.layers.conv2d(conv9, filters=1, kernel_size=[1, 1], padding='same', \
                              activation=None, kernel_regularizer=tf.contrib \
                              .layers.l2_regularizer(0.1), trainable=True, \
                              kernel_initializer=tf.glorot_normal_initializer(), use_bias=True, \
                              name='final')
    output_sigmoid = tf.nn.sigmoid(output, name='final_sigmoid')
    return output_sigmoid


def sift(image, kp_before):
    SIFT = cv2.xfeatures2d.SIFT_create()
    kp, des = SIFT.compute(image, kp_before)
    if des is not None:
        eps = 1e-7
        des /= (des.sum(axis=1, keepdims=True) + eps)
        des = np.sqrt(des)
    return kp, des


def non_max_suppression(image, size_filter, proba):
    non_max = peak_local_max(image, min_distance=size_filter, threshold_abs=proba, \
                             exclude_border=True, indices=False)
    kp = np.where(non_max > 0)
    if len(kp[0]) != 0:
        for i in range(len(kp[0])):

            window = non_max[kp[0][i] - size_filter:kp[0][i] + (size_filter + 1), \
                     kp[1][i] - size_filter:kp[1][i] + (size_filter + 1)]
            if np.sum(window) > 1:
                window[:, :] = 0
    return non_max


class GLAMpoints:
    def __init__(self, **kwargs):
        self.path_weights = str(kwargs['path_glam_weights'])
        self.nms = int(kwargs['NMS'])
        self.min_prob = float(kwargs['min_prob'])
        tf.reset_default_graph()
        self.graph = tf.Graph()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess_detector = tf.Session(config=config, graph=self.graph)
        with self.graph.as_default():
            self.inputs = tf.placeholder(tf.float32, [None, None, None, 1], name='inputs')
            self.training = tf.placeholder(tf.bool, name="mode")
            self.kpmap = Unet_model_4(self.inputs, self.training, norm=True)
            saver = tf.train.Saver()
        with self.sess_detector.as_default():
            saver.restore(self.sess_detector, self.path_weights)
        # the network is restored

    def find_and_describe_keypoints(self, image, **kwargs):
        if len(image.shape) != 2:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image_norm = image / np.max(image)
        image_to_feed = np.zeros((1, image_norm.shape[0], image_norm.shape[1], 1))
        image_to_feed[0, :image_norm.shape[0], :image_norm.shape[1], 0] = image_norm

        with self.sess_detector.as_default():
            kp_map = self.sess_detector.run(self.kpmap, {self.inputs: image_to_feed, self.training: False})
        kp_map_nonmax = non_max_suppression(kp_map[0, :, :, 0], self.nms, self.min_prob)

        keypoints_map = np.where(kp_map_nonmax > 0)
        kp_array = np.array([keypoints_map[1], keypoints_map[0]]).T
        kp_cv2 = [cv2.KeyPoint(kp_array[i, 0], kp_array[i, 1], 10) for i in range(len(kp_array))]
        kp, des = sift(np.uint8(image), kp_cv2)
        kp = np.array([m.pt for m in kp], dtype=np.int32)
        return kp, des


def get_kp_reti(image_color, glampoints, save_path, name):
    if kwarg['green_channel']:
        image = image_color[:, :, 1]
    else:
        image = cv2.cvtColor(image_color, cv2.COLOR_BGR2GRAY)
    kp, des = glampoints.find_and_describe_keypoints(image)

    image_kp = np.uint8(draw_keypoints(cv2.cvtColor(image_color, cv2.COLOR_RGB2BGR), kp))
    cv2.imwrite('{}/{}_reti.png'.format(save_path, name), image_kp)
    print('saved {}'.format(name))
    return kp


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Computing metrics')
    parser.add_argument('--path_images', type=str,
                        help='Directory where to find the images.')
    parser.add_argument('--write_dir', type=str,
                        help='Directory where to write output text containing the metrics')
    parser.add_argument('--path_glam_weights', type=str, default='weights/model-34',
                        help='Path to pretrained weights file of reti model (default: weights/model-34).')

    parser.add_argument('--NMS', type=int, default=10,
                        help='Value of the NMS window applied on the score map output of RetiNet (default:10)')
    parser.add_argument('--min_prob', type=float, default=0.0,
                        help='Minimum probability of a keypoint for RetiNet (default:0)')
    parser.add_argument('--save_text', type=bool, default=False,
                        help='Save matrix of extracted kp as a directory in a text file (default:False)')
    parser.add_argument('--green_channel', type=bool, default=True,
                        help='Use the green channel (default:True)')

    '''
    parser.add_argument('--preprocessing', type=bool, default=False,
    help='Applying preprocessing on the images ? (default: False).')
    parser.add_argument('--preprocessing_equalization', type=int, default=8,
    help='Size of the histogram equalisation window applied on images during preprocessing (default: 8).')
    parser.add_argument('--preprocessing_blurring', type=int, default=2,
    help='Size of the bilinear blurring kernel applied on images during preprocessing (default: 2).')
    '''

    opt = parser.parse_args()
    kwarg = vars(parser.parse_args(args=None, namespace=None))
    glampoints = GLAMpoints(**kwarg)
    kp_dict = {}

    if os.path.isdir(opt.path_images):
        path_to_images=[os.path.join(opt.path_images, f) for f in sorted(os.listdir(opt.path_images))]
    else:
        path_to_images=[]
        path_to_images.append(opt.path_images)

    for i, path_file in enumerate(path_to_images):
        print(path_file)
        try:
            image = cv2.imread(path_file)
            if image is None:
                continue
        except:
            continue

        kp = get_kp_reti(image, glampoints, opt.write_dir,
                         '{}_{}'.format(os.path.basename(os.path.normpath(opt.path_images)), i))
        kp_dict[i] = kp.tolist()
        with open('{}/kp_image{}.txt'.format(kwarg['write_dir'], i), 'w') as outfile:
            outfile.write('{} {}\n'.format(len(kp), len(kp)))
            for m in range(len(kp)):
                outfile.write('{} {}\n'.format(kp[m, 0], kp[m, 1]))