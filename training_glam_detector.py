#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import math

import tensorflow as tf
import cv2
import os
import random
import shutil
import argparse
import yaml
from sklearn.utils import shuffle
from util.utils_CNN import warp_kp, batch, plot_training, find_true_positive_matches, sift, get_sift
from dataset.synthetic_dataset import augment_images, read_from_file
from util.metrics_comparison import get_repeatability, compute_homography, homography_is_accepted, \
    class_homography, compute_registration_error
from model_CNN import non_max_suppression, Unet_model_4


def compute_reward(image1, image2, kp_map1, kp_map2, homographies, nms, distance_threshold=5, compute_metrics=False):
    reward_batch1 = np.zeros((image1.shape), np.float32)
    mask_batch1 = np.zeros((image1.shape), np.float32)

    metrics_per_image = {}
    SIFT = cv2.xfeatures2d.SIFT_create()

    # computes the reward and mask for each element of the batch
    for i in range(kp_map1.shape[0]):
        # for storing information
        plot = {}
        metrics = {}

        # reward an homography of current image pair
        reward1 = reward_batch1[i, :, :, 0]
        homography = homographies[i, :, :]

        # apply NMS to the score map to get the final kp
        kp_map1_nonmax = non_max_suppression(kp_map1[i, :, :, 0], nms, 0)
        kp_map2_nonmax = non_max_suppression(kp_map2[i, :, :, 0], nms, 0)
        keypoints_map1 = np.where(kp_map1_nonmax > 0)
        keypoints_map2 = np.where(kp_map2_nonmax > 0)

        # transform numpy point to cv2 points and compute the corresponding descriptors
        kp1_array = np.array([keypoints_map1[1], keypoints_map1[0]]).T
        kp2_array = np.array([keypoints_map2[1], keypoints_map2[0]]).T
        kp1_cv2 = [cv2.KeyPoint(kp1_array[i, 0], kp1_array[i, 1], 10) for i in range(len(kp1_array))]
        kp2_cv2 = [cv2.KeyPoint(kp2_array[i, 0], kp2_array[i, 1], 10) for i in range(len(kp2_array))]

        kp1, des1 = sift(SIFT, np.uint8(image1[i, :, :, 0]), kp1_cv2)
        kp2, des2 = sift(SIFT, np.uint8(image2[i, :, :, 0]), kp2_cv2)

        # reconverts the cv2 kp into numpy, because descriptor might have removed points
        kp1 = np.array([m.pt for m in kp1], dtype=np.int32)
        kp2 = np.array([m.pt for m in kp2], dtype=np.int32)

        # compute the reward and the mask
        if des1 is not None and des2 is not None:
            if des1.shape[0] > 2 and des2.shape[0] > 2:
                # match descriptors
                bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
                matches1 = bf.match(des1, des2)
                tp, fp, true_positive_matches1, kp1_true_positive_matches, kp1_false_positive_matches = \
                    find_true_positive_matches(kp1, kp2, matches1, homography, distance_threshold=distance_threshold)

                # reward and mask equal to 1 at the position of the TP keypoints
                reward1[kp1_true_positive_matches[:, 1].tolist(), kp1_true_positive_matches[:, 0].tolist()] = 1
                mask_batch1[
                    i, kp1_true_positive_matches[:, 1].tolist(), kp1_true_positive_matches[:, 0].tolist(), 0] = 1
                if tp >= fp:
                    # if there are more tp than fp, backpropagate through all matches
                    mask_batch1[i, kp1_false_positive_matches[:, 1].tolist(),
                                kp1_false_positive_matches[:, 0].tolist(), 0] = 1
                else:
                    # otherwise, find a subset of the fp matches of the same size than tp
                    index = random.sample(range(len(kp1_false_positive_matches)), tp)
                    mask_batch1[i, kp1_false_positive_matches[index, 1].tolist(),
                                kp1_false_positive_matches[index, 0].tolist(), 0] = 1

        if compute_metrics:
            # compute metrics as an indication and plot the different steps
            # metrics about estimated homography
            computed_H, ratio_inliers = compute_homography(kp1, kp2, des1, des2, 'SIFT', 0.80)
            tf_accepted = homography_is_accepted(computed_H)
            RMSE, MEE, MAE = compute_registration_error(homography, computed_H, kp_map1[i, :, :, 0].shape)
            found_homography, acceptable_homography = class_homography(MEE, MAE)
            metrics['computed_H'] = computed_H
            metrics['homography_correct'] = tf_accepted
            metrics['inlier_ratio'] = ratio_inliers
            metrics['class_acceptable'] = acceptable_homography

            # repeatability
            if (kp1.shape[0] != 0) and (kp2.shape[0] != 0):
                repeatability = get_repeatability(kp1, kp2, homography, kp_map1[i, :, :, 0].shape)
            else:
                repeatability = 0
            metrics['repeatability'] = repeatability

            # original kp after NMS
            # results same shape than np.where,
            # Nx2, [0] contains coordinate in vertical direction, [1] in horizontal direction (corresponds to x)
            plot['keypoints_map1'] = keypoints_map1
            plot['keypoints_map2'] = keypoints_map2
            metrics['nbr_kp1'] = len(keypoints_map1[0])
            metrics['nbr_kp2'] = len(keypoints_map2[0])

            # true positive kp: results same shape than np.where,
            # Nx2, [0] contains coordinate in vertical direction, [1] in horizontal direction (corresponds to x)
            tp_kp1 = kp1_true_positive_matches.T[[1,0], :]

            # warped tp kp: results same shape than np.where,
            # Nx2, [0] contains coordinate in vertical direction, [1] in horizontal direction (corresponds to x)
            if len(tp_kp1[1]) != 0:
                where_warped_tp_kp1 = warp_kp(tp_kp1, homography, (kp_map1.shape[1], kp_map1.shape[2]))
            else:
                where_warped_tp_kp1 = np.zeros((2, 1))

            plot['tp_kp1'] = tp_kp1
            plot['warped_tp_kp1'] = where_warped_tp_kp1
            metrics['total_nbr_kp_reward1'] = np.sum(reward1)

            metrics['to_plot'] = plot
            metrics_per_image['{}'.format(i)] = metrics

    return reward_batch1, mask_batch1, metrics_per_image


def training_no_test_set(cfg, plot):
    save_path = os.path.join(cfg['write_dir'], cfg['name_exp'])
    # creates the directory to save the images and checkpoints
    os.makedirs(save_path, exist_ok=True)
    shutil.copy2(os.path.basename(__file__), save_path)

    # load training and testing dataset
    try:
        original_images = read_from_file(cfg['path_images_training'], cfg['augmentation'], False)
    except:
        print('wrong directory of images for training')
        raise SystemExit

    if len(original_images)==0:
        print('Something was wrong when loading the training set, check your directory')
        raise SystemExit

    if cfg['testing']['testing']:
        save_path_test = os.path.join(cfg['write_dir'], cfg['name_exp'], 'test')
        # creates the directory to save the images and checkpoints
        os.makedirs(save_path_test, exist_ok=True)
        # load the original test images
        try:
            original_test_images = read_from_file(cfg['path_images_testing'], cfg['augmentation'], False)
            test_batch_size = cfg['testing']['batch_size']
            nbr_test_batches = math.ceil(original_test_images.shape[0]/test_batch_size)
            loss_testing = []
            test_rep_SIFT = []
            test_homo_SIFT = []
            test_acceptable_homo_SIFT = []
        except ValueError:
            print('wrong directory for images used for testing')
            raise SystemExit

    # read information from the yaml file
    nbr = 0
    epochs = cfg['training']['nbr_epochs']
    batch_size = cfg['training']['batch_size']
    distance_threshold = cfg['training']['distance_threshold']
    batch_number = math.ceil(original_images.shape[0] / batch_size)
    loss_training = []

    g_detector = tf.Graph()
    config = tf.ConfigProto(log_device_placement=True)
    config.gpu_options.allow_growth = True
    sess_detector = tf.Session(config=config, graph=g_detector)
    with g_detector.as_default():
        tf.set_random_seed(11)
        # define the loss function
        inputs = tf.placeholder(tf.float32, [None, None, None, 1], name='inputs')
        reward = tf.placeholder(tf.float32, [None, None, None, 1], name='reward')
        mask = tf.placeholder(tf.float32, [None, None, None, 1], name='mask')
        learning_rate = tf.placeholder(tf.float32, shape=[])
        training = tf.placeholder(tf.bool, name="mode")

        kpmap = Unet_model_4(inputs, training, norm=True)
        kpmap = tf.clip_by_value(kpmap, 1e-8, 1)
        # kpmap is the score map, equal to the probability for each pixel to be a good kp

        loss_matrix = tf.square(reward - kpmap) * mask
        loss = tf.div_no_nan(loss_matrix,
                             tf.reduce_sum(mask, axis=[1, 2, 3]), name='division')

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_step = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

        # define the parameters to save files
        init_op = tf.global_variables_initializer()
        saver = tf.train.Saver(max_to_keep=30, keep_checkpoint_every_n_hours=2)

    if cfg['training']['load_pre_training']:
        with sess_detector.as_default():
            saver.restore(sess_detector, tf.train.latest_checkpoint(save_path))
            # search for checkpoint file

    else:
        with sess_detector.as_default():
            sess_detector.run(init_op)

    for epoch in range(0, epochs):

        print('Starting epoch {}...'.format(epoch))

        original_images = shuffle(original_images, random_state=10)
        learning_rate_value = cfg['training']['learning_rate']
        loss_training_epoch = []

        # as an indication
        rep_glampoints = []
        homo_glampoints = []
        acceptable_homo_glampoints = []

        if epoch == 0:
            nms = cfg['training']['NMS_epoch0']
        else:
            nms = cfg['training']['NMS_others']

        for b in range(batch_number):
            print('minibatch {}'.format(b))

            # creates mini-batches out of original images
            original_images_batch = batch(original_images, batch_size, b)
            # creates batch of synthetic pairs of images, image1 and image2 are augmented,
            # image1_normed and image2_normed are on top normalized to [0-1].
            image1, image2, image1_normed, image2_normed, homographies = \
                augment_images(original_images_batch, cfg, seed=((epoch + 1) * (b + 2)))

            # loss computation and backpropagation through image1
            with sess_detector.as_default():
                kp_map2 = sess_detector.run(kpmap, {inputs: image2_normed, training: True})
                kp_map1 = sess_detector.run(kpmap, {inputs: image1_normed, training: True})
            computed_reward1, mask_batch1, metrics_per_image = \
                compute_reward(image1, image2, kp_map1, kp_map2, homographies, nms,
                               distance_threshold=distance_threshold, compute_metrics=plot)

            with sess_detector.as_default():
                opt1, tr_loss = sess_detector.run([train_step, loss], feed_dict={learning_rate: learning_rate_value,
                                                                                reward: computed_reward1,
                                                                                inputs: image1_normed,
                                                                                mask: mask_batch1, training: True})

            if plot and b % cfg["training"]["plot_every_x_batches"] == 0:
                plot_training(image1, image2, kp_map1, kp_map2, computed_reward1, tr_loss, mask_batch1,
                              metrics_per_image, nbr, epoch, save_path,
                              name_to_save='epoch{}_batch{}.jpg'.format(epoch, b))

            # compute this as an indication for the training
            for j in range(len(image1)):
                rep_glampoints.append(metrics_per_image['{}'.format(j)]['repeatability'])
                homo_glampoints.append(metrics_per_image['{}'.format(j)]['homography_correct'])
                acceptable_homo_glampoints.append(metrics_per_image['{}'.format(j)]['class_acceptable'])
            loss_training_epoch.append(np.sum(tr_loss))

        loss_training.append(np.sum(loss_training_epoch))
        # as an indication
        file_metrics_training = open(os.path.join(save_path_test, 'metrics_training.txt'), 'a')
        file_metrics_training.write('epoch {} \n'.format(epoch))
        file_metrics_training.write('training: TF CNN, {} homography found on {}, {} are of \
        acceptable class, rep {}, loss normalised by the number of matches {}\n'.format(np.sum(homo_glampoints),
                                                                                        len(homo_glampoints),
                                                                                        np.sum(acceptable_homo_glampoints),
                                                                                        np.mean(rep_glampoints),
                                                                                        np.sum(loss_training_epoch)))

        # save the model
        if epoch == 0:
            with sess_detector.as_default():
                saver.save(sess_detector, "{}/model".format(save_path), global_step=epoch)
        else:
            with sess_detector.as_default():
                saver.save(sess_detector, "{}/model".format(save_path), global_step=epoch, write_meta_graph=False)



        # TESTING
        if cfg['testing']['testing'] and epoch % cfg["testing"]["testing_every"] == 0:
            # to gather data
            test_rep_tf = []
            test_homo_tf = []
            test_acceptable_homo_tf = []
            loss_testing_epoch = []

            for b_t in range(nbr_test_batches):
                print('testing mini-batch {}'.format(b_t))
                original_test_images_batch = batch(original_test_images, test_batch_size, b_t)
                test_image1, test_image2, test_image1_normed, test_image2_normed, test_homographies = \
                    augment_images(original_test_images_batch, cfg, seed=(b_t+1))

                # loss computation and backpropagation through image1
                with sess_detector.as_default():
                    test_kp_map2 = sess_detector.run(kpmap, {inputs: test_image2_normed, training: False})
                    test_kp_map1 = sess_detector.run(kpmap, {inputs: test_image1_normed, training: False})
                test_computed_reward1, test_mask_batch1, test_metrics_per_image = \
                    compute_reward(test_image1, test_image2, test_kp_map1, test_kp_map2, test_homographies, cfg['testing']['NMS'],
                                   distance_threshold=distance_threshold, compute_metrics=plot)
                with sess_detector.as_default():
                    test_loss = sess_detector.run(loss, feed_dict={learning_rate: learning_rate_value,
                                                                  reward: test_computed_reward1,
                                                                  inputs: test_image1_normed,
                                                                  mask: test_mask_batch1, training: False})

                '''
                if plot and b_t == 0:
                    plot_training(test_image1, test_image2, test_kp_map1, test_kp_map2, test_computed_reward1, test_loss,
                                  test_mask_batch1, test_metrics_per_image, nbr, epoch, save_path_test,
                                  name_to_save='epoch{}_test_batch{}.jpg'.format(epoch, b_t))
                '''

                for j in range(len(test_image1)):
                    test_rep_tf.append(test_metrics_per_image['{}'.format(j)]['repeatability'])
                    test_homo_tf.append(test_metrics_per_image['{}'.format(j)]['homography_correct'])
                    test_acceptable_homo_tf.append(test_metrics_per_image['{}'.format(j)]['class_acceptable'])
                loss_testing_epoch.append(np.sum(test_loss))

            # give the results of the loss + write metrics in file
            loss_testing.append(np.sum(loss_testing_epoch))
            file_metrics_training.write('test: TF CNN, {} homography found on {}, {} are of \
            acceptable class, rep {}, loss normalised by the number of matches {}\n\n '.format(np.sum(test_homo_tf),
            len(test_homo_tf), np.sum(test_acceptable_homo_tf), np.mean(test_rep_tf), np.sum(loss_testing_epoch)))

            if epoch == 0:
                # compute testing for SIFT only once
                test_accepted_SIFT, test_repeatability_SIFT, test_SIFT_class_acceptable=\
                    get_sift(test_image1, test_image2, test_homographies)
                test_rep_SIFT.extend(test_repeatability_SIFT)
                test_homo_SIFT.extend(test_accepted_SIFT)
                test_acceptable_homo_SIFT.extend(test_SIFT_class_acceptable)
                file_metrics_training.write('test: SIFT, {} homography found on {}, {} are of acceptable class, rep {}\n'.format(
                    np.sum(test_homo_SIFT), len(test_homo_SIFT), np.sum(test_acceptable_homo_SIFT), np.mean(test_rep_SIFT)))

        file_metrics_training.close()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Training the detector')
    parser.add_argument('--path_ymlfile', type=str,
                        help='Path to yaml file.')
    parser.add_argument('--plot', type=bool, default=False,
                        help='Plot during training? (default: False).')

    opt = parser.parse_args()

    with open(opt.path_ymlfile, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    tf.set_random_seed(11)
    np.random.seed(0)
    random.seed(0)

    training_no_test_set(cfg, opt.plot)
