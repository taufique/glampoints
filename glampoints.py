"""
== Preprocessing ==

- Load data
- Crop and pad samples to 256x256

== Model ==
- Create 4-level deep Unet with batch normalization model f 


== Data selection ==
- select image I and I'
- compute transformation g and g'
- transform image I and I' with g and g' respectively
- compute relation between images  I->I': g' * g^-1
- augment: gaussian noise, changes of contrast, illumination, gamma, motion blur and the inverse of image

== Training ==

- compute S=f(I) and S'=f(I')
- compute NMS for both (N, N')
- compute root Sift descriptor N, N' (D, D')
- match points with (N, D), (N', D')
- find true positives and false positives, where true positives fullfill ||H*x - x'|| <= epsilon for epsilon=3
- compute reward map R
- compute mask M
- compute loss as L = sum((f(I)-R)**2)*M) / sum(M)

"""