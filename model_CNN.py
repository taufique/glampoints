#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
from skimage.feature import peak_local_max


def conv_conv_pool(input_layer, filters, training, initializer, pool, batch_norm, name):
    np.random.seed(0)
    np.random.RandomState(0)
    
    if batch_norm is True:
        conv1= tf.layers.conv2d(inputs=input_layer,filters=filters,kernel_size=[3, 3],\
                         padding="same",activation=None, kernel_regularizer=tf.contrib\
                         .layers.l2_regularizer(0.1), trainable=True,\
                         kernel_initializer=initializer, use_bias=True,\
                         name='conv_{}_1'.format(name) )
        batch_conv1 = tf.layers.batch_normalization(conv1, training=training, name='batch_{}_1'.format(name))
        relu1 = tf.nn.relu(batch_conv1, name="relu_{}_1".format(name))
        conv2= tf.layers.conv2d(inputs=relu1,filters=filters,kernel_size=[3, 3],\
                         padding="same",activation=None, kernel_regularizer=tf.contrib.\
                         layers.l2_regularizer(0.1),trainable=True,\
                         kernel_initializer=initializer, use_bias=True,\
                         name='conv_{}_2'.format(name) )
        batch_conv2 = tf.layers.batch_normalization(conv2, training=training, name='batch_{}_2'.format(name))
        relu2 = tf.nn.relu(batch_conv2, name="relu_{}_2".format(name))
    else:
        
        conv1= tf.layers.conv2d(inputs=input_layer,filters=filters,kernel_size=[3, 3],\
                         padding="same",activation=None, kernel_regularizer=tf.contrib\
                         .layers.l2_regularizer(0.1), trainable=True,\
                         kernel_initializer=initializer, use_bias=True,\
                         name='conv_{}_1'.format(name) )
        relu1 = tf.nn.relu(conv1, name="relu_{}_1".format(name))
        conv2= tf.layers.conv2d(inputs=relu1,filters=filters,kernel_size=[3, 3],\
                         padding="same",activation=None, kernel_regularizer=tf.contrib.\
                         layers.l2_regularizer(0.1),trainable=True,\
                         kernel_initializer=initializer, use_bias=True,\
                         name='conv_{}_2'.format(name) )
        relu2 = tf.nn.relu(conv2, name="relu_{}_2".format(name))
    
    if pool is False:
        return relu2
    else:
        pool=tf.layers.max_pooling2d(inputs=relu2, pool_size=[2, 2], strides=2, \
                                      padding='SAME', name='pool_{}'.format(name))
        return relu2, pool


def get_kernel_size(factor):
    """
    Find the kernel size given the desired factor of upsampling.
    """
    return 2 * factor - factor % 2


def upsample_filt(size):
    """
    Make a 2D bilinear kernel suitable for upsampling of the given (h, w) size.
    """
    factor = (size + 1) // 2
    if size % 2 == 1:
        center = factor - 1
    else:
        center = factor - 0.5
    og = np.ogrid[:size, :size]
    return (1 - abs(og[0] - center) / factor) * \
           (1 - abs(og[1] - center) / factor)


def bilinear_upsample_weights(factor, n_filters):
    """
    Create weights matrix for transposed convolution with bilinear filter
    initialization.
    """
    
    filter_size = get_kernel_size(factor)
    
    weights = np.zeros((filter_size,
                        filter_size,
                        n_filters,
                        n_filters*2), dtype=np.float32)
    #shape = [filter_size, filter_size, out_channels, in_channels]
    
    upsample_kernel = upsample_filt(filter_size)
    
    for i in range(n_filters):
        
        weights[:, :, i, i] = upsample_kernel
    
    return weights


def upconv_concat(input_A, input_B, n_filter, training, initializer, batch_norm, name):
    """Upsample `inputA` and concat with `input_B`
    Args:
        input_A (4-D Tensor): (N, H, W, C)
        input_B (4-D Tensor): (N, 2*H, 2*H, C2)
        name (str): name of the concat operation
    Returns:
        output (4-D Tensor): (N, 2*H, 2*W, n_filter + C2)
    """
    np.random.seed(0)
    np.random.RandomState(0)

    upscale=tf.layers.conv2d_transpose(inputs=input_A, filters=n_filter, kernel_size=[2,2],\
                                       padding='same', strides=[2,2], kernel_regularizer=\
                                       tf.contrib.layers.l2_regularizer(0.1),data_format='channels_last', \
                                       activation=None, trainable=True, use_bias=True,\
                                       kernel_initializer=initializer, name='decon_{}'.format(name))
    
    A_shape = tf.shape(upscale)
    B_shape = tf.shape(input_B)

# offsets for the top left corner of the crop
    offsets = tf.cast([0, (A_shape[1] - B_shape[1]) // 2, (A_shape[2] - B_shape[2]) // 2, 0], tf.int32)
    size = tf.cast([-1, B_shape[1], B_shape[2], n_filter], tf.int32)
    A_crop = tf.slice(upscale, offsets, size)

    concat=tf.concat([A_crop, input_B], axis=3, name="concat_{}".format(name))
    return concat


def Unet_model_4(input_layer, training, norm):
    initializer=tf.glorot_normal_initializer()
    conv1, pool1=conv_conv_pool(input_layer, 8, training, initializer=initializer, \
                                pool=True, batch_norm=norm, name='1')
    conv2, pool2=conv_conv_pool(pool1, 16, training, initializer=initializer, \
                                pool=True, batch_norm=norm, name='2')
    conv3, pool3=conv_conv_pool(pool2, 32, training, initializer=initializer, \
                                pool=True, batch_norm=norm, name='3')
    conv4, pool4=conv_conv_pool(pool3, 64, training, initializer=initializer, \
                                pool=True, batch_norm=norm, name='4')
    conv5=conv_conv_pool(pool4, 128, training, initializer=initializer, \
                         pool=False, batch_norm=norm, name='5')
    up6=upconv_concat(conv5, conv4, 64, training, initializer=initializer, \
                      batch_norm=norm, name='6')
    conv6=conv_conv_pool(up6, 64, training, initializer=initializer, \
                         pool=False, batch_norm=norm, name='6')
    up7=upconv_concat(conv6, conv3, 32, training, initializer=initializer, \
                      batch_norm=norm, name='7')
    conv7=conv_conv_pool(up7, 32, training, initializer=initializer, \
                         pool=False, batch_norm=norm, name='7')
    up8=upconv_concat(conv7, conv2, 16, training, initializer=initializer, \
                      batch_norm=norm, name='8')
    conv8=conv_conv_pool(up8, 16, training, initializer=initializer, \
                         pool=False, batch_norm=norm, name='8')
    up9=upconv_concat(conv8, conv1, 8, training, initializer=initializer, \
                      batch_norm=norm, name='9')
    conv9=conv_conv_pool(up9, 8, training, initializer=initializer, \
                         pool=False, batch_norm=norm, name='9')
    output=tf.layers.conv2d(conv9, filters=1, kernel_size=[1,1], padding='same', \
                        activation=None, kernel_regularizer=tf.contrib\
                        .layers.l2_regularizer(0.1), trainable=True, \
                        kernel_initializer=tf.glorot_normal_initializer(), use_bias=True,\
                        name='final' ) 
    output_sigmoid=tf.nn.sigmoid(output, name='final_sigmoid')
    return output_sigmoid


def non_max_suppression(image, size_filter, proba):
    non_max=peak_local_max(image, min_distance=size_filter, threshold_abs=proba, \
                      exclude_border=True, indices=False)
    kp=np.where(non_max>0)
    if len(kp[0]) != 0:
        for i in range(len(kp[0]) ):

            window=non_max[kp[0][i]-size_filter:kp[0][i]+(size_filter+1), \
                           kp[1][i]-size_filter:kp[1][i]+(size_filter+1)]
            if np.sum(window)>1:
                window[:,:]=0
    return non_max


def non_max_suppression_2_(prob, size, min_prob=0.5, iou=0.0001, keep_top_k=0):
    """Performs non maximum suppression on the heatmap by considering hypothetical
    bounding boxes centered at each pixel's location (e.g. corresponding to the receptive
    field). Optionally only keeps the top k detections.
    Arguments:
        prob: the probability heatmap, with shape `[H, W]`.
        size: a scalar, the size of the bouding boxes.
        iou: a scalar, the IoU overlap threshold.
        min_prob: a threshold under which all probabilities are discarded before NMS.
        keep_top_k: an integer, the number of top scores to keep.
    """
    with tf.name_scope('box_nms'):
        with tf.device('/cpu:0'):
            pts = tf.to_float(tf.where(tf.greater(prob, min_prob)))
            size = tf.constant(size/2.)
            boxes = tf.concat([pts-size, pts+size], axis=1)
            scores = tf.gather_nd(prob, tf.to_int32(pts))
            #
            indices = tf.image.non_max_suppression(
                        boxes, scores, tf.shape(boxes)[0], iou)
            pts = tf.gather(pts, indices)
            scores = tf.gather(scores, indices)
            if keep_top_k:
                k = tf.minimum(tf.shape(scores)[0], tf.constant(keep_top_k))  # when fewer
                scores, indices = tf.nn.top_k(scores, k)
                pts = tf.gather(pts, indices)
            prob = tf.scatter_nd(tf.to_int32(pts), scores, tf.shape(prob))
            non_max=prob.eval()
            return non_max
